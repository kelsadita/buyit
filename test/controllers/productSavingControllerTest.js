import supertest from 'supertest';
import RandomGen, {RandomDate} from './../utils/RandomGen';
import {ProductSavingColllection} from './../utils/DbUtil';
import {expect} from 'chai';

describe('Product Saving Functionality: ', () => {

  let request;

  before(() => {
    request = supertest('http://localhost:3001');
    return expect(ProductSavingColllection().remove()).to.be.resolved;
  });

  describe('Add Product Saving', () => {

    describe('Add product saving request validation', () => {
      it('Should throw an error if add product request does not present all required params', (done) => {

        let expectedErrorBody = [{ param: 'date', msg: 'Saving date is required.' },
          { param: 'amount', msg: 'Saving amount is required.' },
          { param: 'amount', msg: 'Saving amount is not a number.' },
          { param: 'comment', msg: 'Comment on saving is required.' } ];

        request.post('/products/1234/savings')
          .set('Accept', 'application/json')
          .expect((res) => {
            let responseBody = res.body;
            expect(responseBody).to.be.instanceOf(Array);
            expect(responseBody.length).to.equal(4);
            expect(responseBody).to.eql(expectedErrorBody);
          })
          .expect(400, done);
      });

      it('Should throw an error if add product request does not present some required params', (done) => {

        let expectedErrorBody = [{ param: 'date', msg: 'Saving date is required.' },
          { param: 'comment', msg: 'Comment on saving is required.' } ];

        let requestBody = {amount: 2000};

        request.post('/products/1234/savings')
          .set('Accept', 'application/json')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send(requestBody)
          .expect((res) => {
            let responseBody = res.body;
            expect(responseBody).to.be.instanceOf(Array);
            expect(responseBody.length).to.equal(2);
            expect(responseBody).to.eql(expectedErrorBody);
          })
          .expect(400, done);
      });

      it('Should throw an error if amount is not in integer format', (done) => {

        let expectedErrorBody = [{
          param: 'amount',
          msg: 'Saving amount is not a number.',
          value: 'abc'
        }];

        let requestBody = {
          date: '20-02-2015',
          amount: 'abc',
          comment: 'test saving'
        };

        request.post('/products/1234/savings')
          .set('Accept', 'application/json')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send(requestBody)
          .expect((res) => {
            let responseBody = res.body;
            expect(responseBody).to.be.instanceOf(Array);
            expect(responseBody.length).to.equal(1);
            expect(responseBody).to.eql(expectedErrorBody);
          })
          .expect(400, done);
      });
    });

    describe('CRUD product savings validation', () => {
      let productId = RandomGen.uuid4();

      let addSavingRequestBody = {
        date: RandomDate().toString(),
        amount: RandomGen.integer(1, 100),
        comment: RandomGen.string(100)
      };

      let addUpdateSavingRequestBody = {
        date: RandomDate().toString(),
        amount: RandomGen.integer(1, 100),
        comment: RandomGen.string(100)
      };

      let updatedSavingRequestBody = {
        date: RandomDate().toString(),
        amount: RandomGen.integer(1, 100),
        comment: RandomGen.string(100)
      };

      // TO be used when calling delete saving service.
      let productSavingIdTobeDelete;

      // TO be used when calling update saving service.
      let productSavingIdTobeUpdated;

      it('Should successfully add product saving with product when no such product exists', (done) => {
        request.post('/products/' + productId + '/savings')
          .set('Accept', 'application/json')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send(addSavingRequestBody)
          .expect((res) => {
            let responseBody = res.body;
            expect(responseBody).to.be.instanceOf(Object);
            expect(responseBody).to.have.all.keys('productId', 'savings', '_id');
            expect(responseBody.productId).to.equal(productId);

            expect(responseBody.savings[0].amount).to.equal(addSavingRequestBody.amount.toString());
            expect(responseBody.savings[0].comment).to.equal(addSavingRequestBody.comment.toString());
            expect(responseBody.savings[0].date).to.equal(addSavingRequestBody.date.toString());
          })
          .expect(200, done);
      });

      it('Should successfully update product saving with product when a product with some savings already exists',
        (done) => {
        request.post('/products/' + productId + '/savings')
          .set('Accept', 'application/json')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send(addUpdateSavingRequestBody)
          .expect(204, done);
      });

      it('Should give product details given a product id', (done) => {
        request.get('/products/' + productId)
          .set('Accept', 'application/json')
          .expect((res) => {
            let responseBody = res.body[0];
            expect(responseBody).to.be.instanceOf(Object);
            expect(responseBody).to.have.all.keys('productId', 'savings', '_id');
            expect(responseBody.productId).to.equal(productId);
            expect(responseBody.savings.length).to.equal(2);
            expect(responseBody.savings[0].amount).to.equal(addSavingRequestBody.amount.toString());
            expect(responseBody.savings[0].comment).to.equal(addSavingRequestBody.comment.toString());
            expect(responseBody.savings[0].date).to.equal(addSavingRequestBody.date.toString());
            expect(responseBody.savings[1].amount).to.equal(addUpdateSavingRequestBody.amount.toString());
            expect(responseBody.savings[1].comment).to.equal(addUpdateSavingRequestBody.comment.toString());
            expect(responseBody.savings[1].date).to.equal(addUpdateSavingRequestBody.date.toString());

            productSavingIdTobeDelete = responseBody.savings[0]._id;
            productSavingIdTobeUpdated = responseBody.savings[1]._id;
          })
          .expect(200, done);
      });

      it('Should delete a product savings with given id', (done) => {
        request.delete('/products/' + productId + '/savings/' + productSavingIdTobeDelete)
          .set('Accept', 'application/json')
          .expect(204, done);
      });

      it('Should give product details given a product id after deleting one saving', (done) => {
        request.get('/products/' + productId)
          .set('Accept', 'application/json')
          .expect((res) => {
            let responseBody = res.body[0];
            expect(responseBody).to.be.instanceOf(Object);
            expect(responseBody).to.have.all.keys('productId', 'savings', '_id');
            expect(responseBody.productId).to.equal(productId);
            expect(responseBody.savings.length).to.equal(1);
            expect(responseBody.savings[0].amount).to.equal(addUpdateSavingRequestBody.amount.toString());
            expect(responseBody.savings[0].comment).to.equal(addUpdateSavingRequestBody.comment.toString());
            expect(responseBody.savings[0].date).to.equal(addUpdateSavingRequestBody.date.toString());
          })
          .expect(200, done);
      });

      it('Should update the product saving with given id', (done) => {
        request.post('/products/' + productId + '/savings/' + productSavingIdTobeUpdated)
          .set('Accept', 'application/json')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send(updatedSavingRequestBody)
          .expect(204, done);
      });

      it('Should give product details given a product id after updating one saving', (done) => {
        request.get('/products/' + productId)
          .set('Accept', 'application/json')
          .expect((res) => {
            let responseBody = res.body[0];
            expect(responseBody).to.be.instanceOf(Object);
            expect(responseBody).to.have.all.keys('productId', 'savings', '_id');
            expect(responseBody.productId).to.equal(productId);
            expect(responseBody.savings.length).to.equal(1);
            expect(responseBody.savings[0].amount).to.equal(updatedSavingRequestBody.amount.toString());
            expect(responseBody.savings[0].comment).to.equal(updatedSavingRequestBody.comment.toString());
            expect(responseBody.savings[0].date).to.equal(updatedSavingRequestBody.date.toString());
          })
          .expect(200, done);
      });

    });
  });
});

describe('Wishlist product specific:', () =>{

  let request;

  before(() => {
    request = supertest('http://localhost:3001');
    return expect(ProductSavingColllection().remove()).to.be.resolved;
  });

  describe('Add Product', () => {

    let PRODUCT_ID_TO_ADD = RandomGen.string(5);

    it('should add product ' +PRODUCT_ID_TO_ADD+ ' successfully', done => {
      request.post('/products/' + PRODUCT_ID_TO_ADD)
        .expect(200, done);
    });

    it('should retrieve added product ' + PRODUCT_ID_TO_ADD + ' successfully', done => {
      request.get('/products/' + PRODUCT_ID_TO_ADD)
        .expect(res => {
          let responseBody = res.body[0];
          expect(responseBody.productId).to.equal(PRODUCT_ID_TO_ADD);
        })
        .expect(200, done);
    });

    it('It should throw an error for adding product which already exists in wish list', done => {
      request.post('/products/' + PRODUCT_ID_TO_ADD)
        .expect(res => {
          let responseBody = res.body;
          expect(responseBody.name).to.equal('EntityAlreadyExists');
          expect(responseBody.statusCode).to.equal(400);
          expect(responseBody.message).to.equal('Product already exists in wish list');
        })
        .expect(400, done);
    });
  });
});