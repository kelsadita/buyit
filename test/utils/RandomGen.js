import Random from 'random-js';

let randomizer = new Random(Random.engines.mt19937().autoSeed());
export default randomizer;

export function RandomDate() {
  return randomizer.date(new Date('06/06/2013'), new Date('06/26/2015'));
}

