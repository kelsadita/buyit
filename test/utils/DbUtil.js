import monk from 'monk';
let db = monk('localhost:27017/test-buyit');
let productSavings = db.get('product');

let productSavingsCollection = {
  remove() {
    return productSavings.remove({});
  }
};

export function ProductSavingColllection() {
  return productSavingsCollection;
}