# BuyIt
---

Application for keeping track of your savings for a product you wish to buy.

## Workflow

1. Search product and add it to wish list
![Screen Shot 2015-09-19 at 2.10.49 pm.png](https://bitbucket.org/repo/9bjEgB/images/3108098951-Screen%20Shot%202015-09-19%20at%202.10.49%20pm.png)
2. Click on product in wish list for which savings are to be added.
![Screen Shot 2015-09-19 at 2.12.12 pm.png](https://bitbucket.org/repo/9bjEgB/images/2849450184-Screen%20Shot%202015-09-19%20at%202.12.12%20pm.png)
3. Add savings and check your saving progress.
![Screen Shot 2015-09-19 at 2.13.14 pm.png](https://bitbucket.org/repo/9bjEgB/images/1712737305-Screen%20Shot%202015-09-19%20at%202.13.14%20pm.png)
![Screen Shot 2015-09-19 at 2.14.15 pm.png](https://bitbucket.org/repo/9bjEgB/images/4121470766-Screen%20Shot%202015-09-19%20at%202.14.15%20pm.png)
![Screen Shot 2015-09-19 at 2.15.07 pm.png](https://bitbucket.org/repo/9bjEgB/images/4085610487-Screen%20Shot%202015-09-19%20at%202.15.07%20pm.png)

## How do I get set up?

### Summary of set up

This application is built using following technologies,

*Back-End*

1. Express (Node.js framework)
2. mongoDB (for database)
3. Amazon Product API (for searching products and getting their details)
   
*Front-End*

1. React.js
2. Flux (application architecture) 
3. Twitter Bootstrap (both css and js)
4. ES6 (using babel)

### Dependencies

Make sure you have following things installed in your machine,

1. Node.js (https://nodejs.org/en/)
2. mongoDB (http://docs.mongodb.org/master/installation/)

### Configuration
Make sure to add app-config.js in directory `BuyIt/config/app-config.js` containing following details,

```
#!javascript
var config = {};
 
 // AWS product advertising API specific configs
 config.awsId = '<aws id>';
 config.awsSecret = '<aws secret>';
 
 // Database configurations
 config.mongodb = {
   dev: {
     url: 'localhost:27017/test-buyit'
   },
   production: {
     url: 'localhost:27017/buyit'
   }
 };
 
 module.exports = config;

```

*Note:* You can register to aws product advertising API by following [these](http://docs.aws.amazon.com/AWSECommerceService/latest/DG/becomingDev.html) guideline

## How to run application locally?

1. `cd` into project root.
2. Build the project using webpack `npm run webpack`
3. Run the express.js server `npm start`
4. Also make sure to start the mongo demon `mongod --dbpath=data`
 
## For development
 
1. `cd` into project root.
2. Build the project in watch mode using webpack in one terminal window: `npm run webpack`
3. In other terminal window run express.js server in development mode: `npm run devmode`
 
## TODO

1. Test cases for React.js components
2. Full test coverage for backend web services.
3. `Add Debt` functionality
4. Update UI to fix `add saving` modal (date input box is not aligned properly)
5. When a product having some savings is to be removed from wish list, shift its savings to some other product