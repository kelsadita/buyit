import productSavingValidators from './../validators/productSavingValidators';
import productSavingService from './../services/productSavingService';
import Promise from 'bluebird';

import AwsProductService from './../services/awsProductService';

import EntityAlreadyExists from './../exceptions/EntityAlreadyExists';
import EntityFailedToAdd from './../exceptions/EntityFailedToAdd';

let productSavingController = (function(){
  function getProductDetails(req, res) {
    let productId = req.params.productId;
    productSavingService.findProduct(productId, req)
      .success((productSavingDetails) => {
        res.json(productSavingDetails);
      })
      .error((err) => {
        res.send(err);
      });
  }

  function getAllProducts(req, res) {
    productSavingService.findAllProducts(req)
      .success(products => {
        Promise.all(products.map(product => {
          return AwsProductService.fetchProductDetails(product.productId);
        })).then(result => {
          res.json(result);
        }).catch(error => {
          res.send(error);
        });
      })
      .error(err => {
        res.send(err);
      });
  }

  function addProduct(req, res) {
    let productId = req.params.productId;
    productSavingService.findProduct(productId, req)
      .success(productData => {
        if (Array.isArray(productData) && productData.length === 0) {
          productSavingService
            .addProductWithoutSaving(productId, req)
            .success(data => {
              res.status(200).json(data);
            });
        } else {
          // TODO: Create error constant
          let errorObj = new EntityAlreadyExists('Product already exists in wish list');
          res.status(errorObj.statusCode).send(errorObj);
        }
      })
      .error(error => {
        let errorObj = new EntityFailedToAdd('Failed to added given product to wish list', error);
        res.status(error.statusCode).send(errorObj);
      });
  }

  function addUpdateProductSaving(req, res) {
    let errors = productSavingValidators.validateAddProductSavingRequest(req);
    let productId = req.params.productId;
    if(errors) {
      res.status(400).json(errors);
    } else {
      productSavingService.findProduct(productId, req)
        .success((data) => {
          if(Array.isArray(data) && data.length === 0) {
            // TODO: Try to replace following with addProduct function.
            productSavingService.addProduct(productId, req).success((data) => {
              res.status(200).json(data);
            }).error((error) => {
              res.send(error);
            });
          } else {
            productSavingService.addProductSaving(productId, req).success((data) => {
              res.status(204).json(data);
            }).error((error) => {
              res.status(500).send(error);
            });
          }
        })
        .error((error) => {
          res.status(500).send(error);
        });
    }
  }

  function removeProduct(req, res) {
    let productId = req.params.productId;
    productSavingService.removeProduct(productId, req)
      .success((data) => {
        res.status(204).json(data);
      })
      .error((error) => {
        res.status(500).send(error);
      });
  }

  function removeProductSaving(req, res) {
    let productId = req.params.productId;
    let savingId = req.params.savingId;
    productSavingService.removeProductSaving(productId, savingId, req)
      .success((data) => {
        res.status(204).json(data);
      })
      .error((error) => {
        res.status(500).send(error);
      });
  }

  function updateProductSaving(req, res) {
    let productId = req.params.productId;
    let savingId = req.params.savingId;
    productSavingService.updateProductSaving(productId, savingId, req)
      .success((data) => {
        res.status(204).json(data);
      })
      .error((error) => {
        res.status(500).send(error);
      });
  }

  return {
    getProductDetails: getProductDetails,
    getAllProducts: getAllProducts,
    addProduct: addProduct,
    addUpdateProductSaving: addUpdateProductSaving,
    removeProduct: removeProduct,
    removeProductSaving: removeProductSaving,
    updateProductSaving: updateProductSaving
  };

})();

export default productSavingController;