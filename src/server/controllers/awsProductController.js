import AwsProductService from './../services/awsProductService';

let AwsProductController = (function(){
  function searchProduct(req, res) {
    let productSlug = req.params.productSlug;
    AwsProductService
      .searchProduct(productSlug)
      .then((results) => {
        res.status(200).json(results);
      })
      .catch((error) => {
        // TODO: use logging framework
        console.log(error);
        res.status(500).send(error);
      });
  }

  function fetchProductDetails (req, res) {
    let productId = req.params.productId;
    AwsProductService
      .fetchProductDetails(productId)
      .then((result) => {
        res.status(200).json(result);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send(error);
      });
  }

  return {
    searchProduct: searchProduct,
    fetchProductDetails: fetchProductDetails
  };
})();

export default AwsProductController;
