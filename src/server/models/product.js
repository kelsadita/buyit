class Product {

  constructor(productId, title, imageUrl, price) {
    this.productId = productId;
    this.title = title;
    this.imageUrl = imageUrl;
    this.price = price;
  }

}

class ProductBuilder {

  addProductId(productId) {
    this.productId = productId;
    return this;
  }

  addTitle(title) {
    this.title  = title;
    return this;
  }

  addImage(imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  addPrice(price) {
    this.price = price;
    return this;
  }

  build() {
    return new Product(
      this.productId,
      this.title,
      this.imageUrl,
      this.price
    );
  }

}

export default ProductBuilder;
