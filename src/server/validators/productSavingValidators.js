let productSavingValidators = {
  validateAddProductSavingRequest(req) {
    req.assert('date', 'Saving date is required.').notEmpty();
    req.assert('amount', 'Saving amount is required.').notEmpty()
      .isInt().withMessage('Saving amount is not a number.');
    req.assert('comment', 'Comment on saving is required.').notEmpty();
    return req.validationErrors();
  }
};

export default productSavingValidators;
