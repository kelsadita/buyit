import {OperationHelper} from 'apac';
import config from './../../../config/app-config';
import Promise from 'bluebird';

import ProductBuilder from './../models/product';

let opHelper = new OperationHelper({
  awsId: config.awsId,
  awsSecret: config.awsSecret,
  assocId: 'test',
  endPoint: 'webservices.amazon.in',
  version: '2013-08-01'
});

let AwsProductService = (function(){

  /***
   * Search a product on amazon based on given search string and
   * return formatted product objects
   * @param productSlug
   */
  function searchProduct(productSlug) {
    return new Promise((resolve, reject) => {
      opHelper.execute('ItemSearch', {
        'SearchIndex': 'All',
        'Keywords': productSlug,
        'ResponseGroup': 'ItemAttributes,Offers,Images'
      }, (err, result) => {
        if(err) {
          return reject(err);
        }
        let products = result.ItemSearchResponse.Items[0].Item || [];
        products = products.map(product => {
          return getFormattedProduct(product);
        });
        resolve(products);
      });
    });
  }

  /***
   * Get single product details from aws based on given product id
   * and return formatted product object.
   * @param productId
   */
  function fetchProductDetails (productId) {
    return new Promise((resolve, reject) => {
      opHelper.execute('ItemLookup', {
        'IdType': 'ASIN',
        'ItemId': productId,
        'ResponseGroup': 'ItemAttributes,Offers,Images'
      }, (err, results) => {
        if (err) {
          reject(err);
        }
        //console.log(JSON.stringify(results));
        resolve(getFormattedProduct(results.ItemLookupResponse.Items[0].Item[0]));
      });
    });
  }

  /***
   * Format product object returned from aws to domain product object
   * @param product
   * @returns {*}
   */
  function getFormattedProduct(product) {
    let productId = product.ASIN[0];
    let productPrice = product.ItemAttributes[0].ListPrice || [];
    let productTitle = product.ItemAttributes[0].Title[0];
    let productImage = product.LargeImage ? product.LargeImage[0].URL[0]
      : (product.ImageSets
          ? product.ImageSets[0].ImageSet[0].LargeImage[0].URL[0]
          : '');

    return new ProductBuilder()
      .addProductId(productId)
      .addPrice(productPrice)
      .addImage(productImage)
      .addTitle(productTitle)
      .build();
  }

  return {
    searchProduct: searchProduct,
    fetchProductDetails: fetchProductDetails
  };
})();


export default AwsProductService;
