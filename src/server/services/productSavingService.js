import {ObjectID} from 'mongodb';

let ProductSavingService = (function(){

  /***
   * Fetch product with given product ID
   * @param productId
   * @param req
   * @returns {*}
   */
  function findProduct(productId, req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.find({
      productId: productId
    }).success(productData => {
      return productData;
    }).error(error => {
      return error;
    });
  }

  /***
   * Get all products and their savings embeded in it
   * @param req
   * @returns {*}
   */
  function findAllProducts(req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.find();
  }

  /***
   * Add new product + saving
   * @param productId
   * @param req
   * @returns {OrderedBulkOperation|Promise|UnorderedBulkOperation}
   */
  function addProduct(productId, req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.insert({
      productId: productId,
      savings: [
        {
          _id: new ObjectID(),
          amount: req.body.amount,
          date: req.body.date,
          comment: req.body.comment
        }
      ]
    });
  }

  /***
   * Add new saving to existing product
   * @param productId
   * @param req
   * @returns {*}
   */
  function addProductSaving(productId, req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.update({
      productId: productId
    }, {
      $push: {
        savings: {
          _id: new ObjectID(),
          amount: req.body.amount,
          date: req.body.date,
          comment: req.body.comment
        }
      }
    });
  }

  /***
   * Add new product + empty saving
   * used in workflow where we have to add new product to wish list
   * @param productId
   * @param req
   * @returns {OrderedBulkOperation|Promise|UnorderedBulkOperation}
   */
  function addProductWithoutSaving(productId, req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.insert({
      productId: productId,
      savings: []
    });
  }

  /***
   * Remove product from wish list
   * @param productId
   * @param req
   * @returns {*}
   */
  function removeProduct(productId, req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.remove({
      productId: productId
    });
  }

  /***
   * Update product saving
   * @param productId
   * @param savingId
   * @param req
   * @returns {*}
   */
  function updateProductSaving(productId, savingId, req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.update({
      productId: productId,
      'savings._id': new ObjectID(savingId)
    }, {
      $set: {
        'savings.$': {
          _id: new ObjectID(savingId),
          amount: req.body.amount,
          date: req.body.date,
          comment: req.body.comment
        }
      }
    });
  }

  /***
   * Remove product saving
   * @param productId
   * @param savingId
   * @param req
   * @returns {*}
   */
  function removeProductSaving(productId, savingId, req) {
    let db = req.db;
    let productCollection = db.get('product');

    return productCollection.update({
      productId: productId
    }, {
      $pull: {
        'savings': {
          _id: new ObjectID(savingId)
        }
      }
    });
  }

  return {
    findProduct: findProduct,
    findAllProducts: findAllProducts,
    addProduct: addProduct,
    addProductWithoutSaving: addProductWithoutSaving,
    removeProduct: removeProduct,
    addProductSaving: addProductSaving,
    updateProductSaving: updateProductSaving,
    removeProductSaving: removeProductSaving
  };

})();


export default ProductSavingService;
