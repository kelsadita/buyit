import path from 'path';
import {Router} from 'express';
import awsServerService from '../controllers/awsProductController';
import productSavingController from '../controllers/productSavingController';

let router = Router();

router.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../../client/index.html'));
});

router.get('/awsproduct/:productId', awsServerService.fetchProductDetails);
router.get('/awsproduct/search/:productSlug', awsServerService.searchProduct);

router.get('/products', productSavingController.getAllProducts);
router.post('/products/:productId', productSavingController.addProduct);

router.get('/products/:productId', productSavingController.getProductDetails);
router.delete('/products/:productId', productSavingController.removeProduct);

router.post('/products/:productId/savings', productSavingController.addUpdateProductSaving);
router.delete('/products/:productId/savings/:savingId', productSavingController.removeProductSaving);
router.post('/products/:productId/savings/:savingId', productSavingController.updateProductSaving);

export default router;
