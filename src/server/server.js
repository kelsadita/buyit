import path from 'path';
import Express from 'express';
import expressValidator from 'express-validator';
import bodyParser from 'body-parser';
import monk from 'monk';
import routes from './routes/routes';
import config from './../../config/app-config.js';

var app = Express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());

// Setting up assets
const PATH_STYLES = path.resolve(__dirname, '../client/styles');
const PATH_DIST = path.resolve(__dirname, '../../dist');
app.use('/styles', Express.static(PATH_STYLES));
app.use(Express.static(PATH_DIST));

// Make our db accessible to our router
console.log('Application booting in mode: ' + process.env.MODE);
let dbUrl = process.env.MODE === 'dev' ? config.mongodb.dev.url : config.mongodb.production.url;
let db = monk(dbUrl);
app.use(function (req, res, next) {
  req.db = db;
  next();
});

// Setting up the routes
app.use('/', routes);

var server = app.listen(process.env.PORT || 3000, () => {
  var port = server.address().port;
  console.log('Server is listening at %s', port);
});

