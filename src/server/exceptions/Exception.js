class Exception {
  constructor(name, statusCode, message, errorObject) {
    this.name = name;
    this.statusCode = statusCode;
    this.message = message;
    this.errorObject = errorObject;
  }
}

export default Exception;