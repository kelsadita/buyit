import Exception from './Exception';

class EntityFailedToAdd extends Exception {
  constructor(message, errorObject) {
    super('EntityAlreadyExists', 500, message, errorObject);
  }
}

export default EntityFailedToAdd;
