import Exception from './Exception';

class EntityAlreadyExists extends Exception {
  constructor(message, errorObject) {
    super('EntityAlreadyExists', 400, message, errorObject);
  }
}

export default EntityAlreadyExists;