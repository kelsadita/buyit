import AmazonService from '../services/awsService';
import ProductSavingClient from '../services/productSavingClient';
import AppDispatcher from '../dispatcher/AppDispatcher';
import ProductConstants from './../constants/ProductConstants.js';

let productSavingClient = new ProductSavingClient();
let awsServiceClient = new AmazonService();

let ProductActions = (function() {
  function loadProduct(productId) {
    return awsServiceClient
      .loadItem(productId)
      .then((data) => {
        AppDispatcher.dispatch({
          actionType: ProductConstants.RECEIVED_PRODUCT_DATA,
          payload: data
        });
      })
      .catch((err) => {
        // TODO: create error action
        console.log(err);
      });
  }

  function loadProductSavings(productId) {
    return productSavingClient
      .getProductSavings(productId)
      .then((data) => {
        AppDispatcher.dispatch({
          actionType: ProductConstants.RECEIVED_PRODUCT_SAVINGS,
          payload: data
        });
      })
      .catch((err) => {
        // TODO: create error action
        console.log(err);
      });
  }

  function addProductSaving(productId, productSavingData) {
    return productSavingClient
      .addProductSaving(productId, productSavingData)
      .then(() => {
        return productSavingClient.getProductSavings(productId);
      })
      .then((data) => {
        AppDispatcher.dispatch({
          actionType: ProductConstants.RECEIVED_PRODUCT_SAVINGS,
          payload: data
        });
      })
      .catch((err) => {
        // TODO: create error action
        console.log(err);
      });
  }

  function editProductSaving(productSavingEditData) {
    return productSavingClient
      .editProductSaving(productSavingEditData)
      .then(() => {
        return productSavingClient.getProductSavings(productSavingEditData.productId);
      })
      .then((data) => {
        AppDispatcher.dispatch({
          actionType: ProductConstants.RECEIVED_PRODUCT_SAVINGS,
          payload: data
        });
      })
      .catch((err) => {
        // TODO: create error action
        console.log(err);
      });
  }

  function deleteProductSaving(productId, savingId) {
    return productSavingClient
      .deleteProductSaving(productId, savingId)
      .then(() => {
        return productSavingClient.getProductSavings(productId);
      })
      .then((data) => {
        AppDispatcher.dispatch({
          actionType: ProductConstants.RECEIVED_PRODUCT_SAVINGS,
          payload: data
        });
      })
      .catch((err) => {
        // TODO: create error action
        console.log(err);
      });
  }

  return {
    loadProduct: loadProduct,
    loadProductSavings: loadProductSavings,
    addProductSaving: addProductSaving,
    editProductSaving: editProductSaving,
    deleteProductSaving: deleteProductSaving
  };

})();

export default ProductActions;

