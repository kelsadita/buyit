import AppDispatcher from '../dispatcher/AppDispatcher';
import NotificationConstants from '../constants/NotificationConstants';

//import ProductSearchStore from './../stores/ProductSearchStore';
//import ProductWishListStore from './../stores/ProductWishListStore';
//import ProductSavingStore from './../stores/ProductSavingStore';
//import ProductStore from './../stores/ProductStore';

let NotificationActions = (function(){
  function notify(notificationType, notificationMessage) {
    let notificationData = {
      type: notificationType,
      message: notificationMessage
    };

    // TODO: fix following hack using waitFor function of flux
    setTimeout(() => {
      if(!AppDispatcher.isDispatching()){
        AppDispatcher.dispatch({
          actionType: NotificationConstants.RECEIVED_NOTIFICATION,
          payload: notificationData
        });
      }
    }, 0);
  }

  return {
    notify: notify
  };

})();

export default NotificationActions;

