import ProductClient from '../services/ProductClient';
import AppDispatcher from '../dispatcher/AppDispatcher';
import ProductConstants from './../constants/ProductConstants.js';

import NotificationAction from './NotificationActions';
import NotificationConstants from './../constants/NotificationConstants';


let ProductWishListActions = (function() {
  function loadAllProducts() {
    return ProductClient
      .getAllProducts()
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ProductConstants.RECEIVED_ALL_WISH_LIST_PRODUCTS,
          payload: data
        });
      })
      .catch(err => {
        // TODO: create error action
        console.log(err);
      });
  }

  function addProductToWishList(productId) {
    return ProductClient
      .addProductToWishList(productId)
      .then(() => {
        NotificationAction.notify(NotificationConstants.SUCCESS,
          'Product added to <a href="#/wishlist">wish list</a> successfully.');
      })
      .catch(error => {
        NotificationAction.notify(NotificationConstants.FAILED,
          error.res.body.message.replace('wish list',
          '<a href="#/wishlist">wish list</a>'));
      });
  }

  function removeProductFromWishList(productId) {
    return ProductClient
      .removeProductFromWishList(productId)
      .then(() => {
        NotificationAction.notify(NotificationConstants.SUCCESS,
          'Product deleted successfully from wish list.');
        loadAllProducts();
      })
      .catch(() => {
        NotificationAction.notify(NotificationConstants.FAILED,
          'Failed to delete product from wish list.');
      });
  }

  return {
    loadAllProducts: loadAllProducts,
    addProductToWishList: addProductToWishList,
    removeProductFromWishList: removeProductFromWishList
  };

})();

export default ProductWishListActions;


