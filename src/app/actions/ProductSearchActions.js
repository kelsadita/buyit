import AmazonService from '../services/awsService';
import AppDispatcher from '../dispatcher/AppDispatcher';

import ProductConstants from '../constants/ProductConstants';

let awsServiceClient = new AmazonService();


let ProductSearchActions = (function(){
  function loadProductsFromAws(productSlug) {
    return awsServiceClient
      .searchItem(productSlug)
      .then((data) => {
        AppDispatcher.dispatch({
          actionType: ProductConstants.RECEIVED_PRODUCT_SEARCH_RESULTS,
          payload: data
        });
      })
      .catch((err) => {
        // TODO: create error action
        console.log(err);
      });
  }

  return {
    loadProductsFromAws: loadProductsFromAws
  };

})();

export default ProductSearchActions;
