import AppDispatcher from './../dispatcher/AppDispatcher';
import ProductConstants from './../constants/ProductConstants';
import Store from './Store';

let productSavings;

function reset() {
  productSavings = {};
}

class ProductSavingStore extends Store {

  constructor() {
    super();
  }

  getState() {
    return productSavings;
  }
}

let productSavingStoreInstance = new ProductSavingStore();

ProductSavingStore.dispatchToken = AppDispatcher.register((action) => {
  switch (action.actionType) {
    case ProductConstants.RECEIVED_PRODUCT_SAVINGS:
      reset();
      productSavings = action.payload.body;
      break;

    default:
      return;

  }
  productSavingStoreInstance.emitChange();
});

export default productSavingStoreInstance;

