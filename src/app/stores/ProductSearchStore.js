import AppDispatcher from './../dispatcher/AppDispatcher';
import ProductConstants from './../constants/ProductConstants';
import Store from './Store';

// TODO: Worried about this global state, FP :(
let productSearchData;

function reset() {
  productSearchData = [];
}

class ProductSearchStore extends Store {

  constructor() {
    super();
    productSearchData = [];
  }

  getState() {
    return productSearchData;
  }
}

let productSearchStoreInstance = new ProductSearchStore();

ProductSearchStore.dispatchToken = AppDispatcher.register((action) => {
  switch (action.actionType) {
    case ProductConstants.RECEIVED_PRODUCT_SEARCH_RESULTS:
      reset();
      productSearchData = action.payload.body;
      break;

    default:
      return;

  }
  productSearchStoreInstance.emitChange();
});

export default productSearchStoreInstance;


