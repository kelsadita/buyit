import AppDispatcher from './../dispatcher/AppDispatcher';
import ProductConstants from './../constants/ProductConstants';
import Store from './Store';

let productsData;

function reset() {
  productsData = {};
}

class ProductWishListStore extends Store {

  constructor() {
    super();
  }

  getState() {
    return productsData;
  }
}

let productWishListStoreInstance = new ProductWishListStore();

ProductWishListStore.dispatchToken = AppDispatcher.register(action => {
  switch (action.actionType) {
    case ProductConstants.RECEIVED_ALL_WISH_LIST_PRODUCTS:
      reset();
      productsData = action.payload.body;
      break;

    default:
      return;

  }
  productWishListStoreInstance.emitChange();
});

export default productWishListStoreInstance;

