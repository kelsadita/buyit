import AppDispatcher from './../dispatcher/AppDispatcher';
import NotificationConstants from './../constants/NotificationConstants';
import Store from './Store';

let notificationData;

function reset() {
  notificationData = {};
}

class NotificationStore extends Store {

  constructor() {
    super();
    notificationData = {
      type: NotificationConstants.DISMISS,
      message: ''
    };
  }

  getState() {
    return notificationData;
  }
}

let notificationStoreInstance = new NotificationStore();

NotificationStore.dispatchToken = AppDispatcher.register((action) => {
  switch (action.actionType) {
    case NotificationConstants.RECEIVED_NOTIFICATION:
      reset();
      notificationData = action.payload;
      break;

    default:
      return;

  }
  notificationStoreInstance.emitChange();
});

export default notificationStoreInstance;