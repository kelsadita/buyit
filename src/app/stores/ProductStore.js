import AppDispatcher from './../dispatcher/AppDispatcher';
import {EventEmitter} from 'events';
import ProductConstants from './../constants/ProductConstants';
import Store from './Store';

let productDescription;

function reset() {
  productDescription = {};
}

class ProductStore extends Store {

  constructor() {
    super();
  }

  getState() {
    return productDescription;
  }
}

let productStoreInstance = new ProductStore();

ProductStore.dispatchToken = AppDispatcher.register((action) => {
  switch (action.actionType) {
    case ProductConstants.RECEIVED_PRODUCT_DATA:
      reset();
      productDescription = action.payload.body;
      break;

    default:
      return;

  }
  productStoreInstance.emitChange();
});

export default productStoreInstance;
