import request from 'superagent-bluebird-promise';

class ProductClient {
  static getAllProducts() {
    return request
      .get('/products/')
      .promise();
  }

  static addProductToWishList(productId) {
    return request
      .post('/products/' + productId)
      .promise();
  }

  static removeProductFromWishList(productId) {
    return request
      .del('/products/' + productId)
      .promise();
  }
}

export default ProductClient;
