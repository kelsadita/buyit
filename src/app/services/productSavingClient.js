import request from 'superagent-bluebird-promise';

class ProductSavingClient {
  constructor() {

  }

  getProductSavings(productId) {
    return request
      .get('/products/' + productId)
      .promise();
  }

  addProductSaving(productId, productSavingData) {
    return request
      .post('/products/' + productId + '/savings')
      .send(productSavingData)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/x-www-form-urlencoded');
  }

  editProductSaving(productSavingEditData) {

    let productSavingEditBody = {
      date: productSavingEditData.date,
      amount: productSavingEditData.amount,
      comment: productSavingEditData.comment
    };

    return request
      .post('/products/' + productSavingEditData.productId + '/savings/' + productSavingEditData.savingId)
      .send(productSavingEditBody)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/x-www-form-urlencoded');
  }

  deleteProductSaving(productId, savingId) {
    return request
      .del('/products/' + productId + '/savings/' + savingId);
  }
}

export default ProductSavingClient;
