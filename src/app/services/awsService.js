import request from 'superagent-bluebird-promise';
import Debug from 'debug';

class AmazonService {
  constructor() {

  }

  searchItem(itemName) {
    return request
      .get('/awsproduct/search/' + itemName)
      .promise();
  }

  loadItem(itemId) {
    return request
      .get('/awsproduct/' + itemId)
      .promise();
  }
}

export default AmazonService;