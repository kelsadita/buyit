import keyMirror from 'keymirror';

export default keyMirror({
  SUCCESS: null,
  FAILED: null,
  WARNING: null,

  DISMISS: null,

  RECEIVED_NOTIFICATION: null
});

