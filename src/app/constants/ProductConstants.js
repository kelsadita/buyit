import keyMirror from 'keymirror';

export default keyMirror({
  RECEIVED_ALL_WISH_LIST_PRODUCTS: null,
  PRODUCT_ADDED_TO_WISH_LIST: null,

  RECEIVED_PRODUCT_DATA: null,
  RECEIVED_PRODUCT_SAVINGS: null,

  ADD_PRODUCT_SAVING: null,

  RECEIVED_PRODUCT_SEARCH_RESULTS: null
});
