import keyMirror from 'keymirror';

export default keyMirror({
  SEARCH_INITIATED: null,
  SEARCH_COMPLETED: null,
  SEARCH_UI_LOADED: null
});

