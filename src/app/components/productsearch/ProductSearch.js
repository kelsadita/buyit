import React from 'react';
import ProductSearchActions from './../../actions/ProductSearchActions';
import ProductSearchStore from './../../stores/ProductSearchStore';
import ProductSearchResultGrid from './ProductSearchResultGrid';
import ProductSearchEvents from './../../constants/ProductSearchEvents';
import Notification from './../layout/Notification';

class ProductSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productSearchData: props.productSearchData,
      eventType: props.eventType
    };
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    ProductSearchStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  _onChange() {
    if (this.mounted) {
      this.setState({
        productSearchData: ProductSearchStore.getState(),
        eventType: ProductSearchEvents.SEARCH_COMPLETED
      });
    }
  }

  searchProductOnAmazon(event){
    if (event.key === 'Enter') {
      this.setState({
        eventType: ProductSearchEvents.SEARCH_INITIATED
      });
      ProductSearchActions.loadProductsFromAws(event.target.value);
    }
  }

  render() {
    return(
      <div>
        <input type="text"
               className="form-control input-lg"
               placeholder="Search product from amazon.in"
               onKeyDown={this.searchProductOnAmazon.bind(this)}/>
        <br/>
        <Notification />
        <ProductSearchResultGrid
          productSearchData={this.state.productSearchData}
          eventType={this.state.eventType}/>
      </div>
    );
  }
}

ProductSearch.propTypes = {
  productSearchData: React.PropTypes.array,
  eventType: React.PropTypes.string
};

ProductSearch.defaultProps = {
  productSearchData: ProductSearchStore.getState(),
  eventType: ProductSearchEvents.SEARCH_UI_LOADED
};

export default ProductSearch;
