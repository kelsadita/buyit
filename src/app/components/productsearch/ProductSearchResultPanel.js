import React from 'react';
import ProductWishListActions from './../../actions/ProductWishListActions';

class ProductSearchResultPanel extends React.Component {
  constructor(props) {
    super(props);
    this.imageStyle = {
      width: 280,
      height: 360
    };
    this.panelHeadingStyle = {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    };
  }

  addProductToWishList(productId) {
    ProductWishListActions.addProductToWishList(productId);
  }

  render() {

    if(!Object.keys(this.props.productDetails)) {
      return;
    }

    let productId = this.props.productDetails.productId;
    let title = this.props.productDetails.title;
    let productImageUrl = this.props.productDetails.imageUrl;
    let productPrice = this.props.productDetails.price[0].FormattedPrice[0];
    return (
      <div className="col-md-6">
        <div className="panel panel-default">
          <div className="panel-heading"
               style={this.panelHeadingStyle}
               title={title}>
            {title}
          </div>
          <div className="panel-body">
            <img src={productImageUrl}
                 alt={title}
                 style={this.imageStyle}/>
            <hr/>
            <div className="col-md-7">
              <h4>{productPrice}</h4>
            </div>
            <div className="col-md-5">
              <button type="button"
                      className="btn btn-success"
                      aria-label="Left Align"
                      onClick={this.addProductToWishList.bind(this, productId)}>
                    <span className="glyphicon glyphicon-plus"
                          aria-hidden="true">
                    </span>
                Wishlist
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProductSearchResultPanel.propTypes = {
  productDetails: React.PropTypes.object,
  key: React.PropTypes.number
};

ProductSearchResultPanel.defaultProps = {
  productDetails: {},
  key: 0
};


export default ProductSearchResultPanel;
