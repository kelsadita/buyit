import React from 'react';
import ProductSearchEvents from './../../constants/ProductSearchEvents';
import ProductSearchResultPanel from './ProductSearchResultPanel';
import NotificationActions from './../../actions/NotificationActions';
import NotificationConstants from './../../constants/NotificationConstants';

class ProductSearchResultGrid extends React.Component{
  constructor(props) {
    super(props);
  }

  getSearchResultsPanels(searchedProductResultList){
    if(searchedProductResultList.length === 0) {
      NotificationActions.notify(NotificationConstants.FAILED,
        'Oops! no product with given description.');
    } else {

      // Check if all the prices are available
      searchedProductResultList = this.props.productSearchData
        .filter(product => product.price.length !== 0);

      // otherwise throw no such product error
      if(searchedProductResultList.length === 0) {
        NotificationActions.notify(NotificationConstants.FAILED,
          'Oops! no product with given description.');
      } else {
        return (
          <div>
            {
              searchedProductResultList.map((product, index) =>
                <ProductSearchResultPanel productDetails={product} key={index}/>)
            }
          </div>
        );
      }
    }

    // Always send empty component if non of the above follows
    return (<div></div>);
  }

  render() {
    switch (this.props.eventType) {
      case ProductSearchEvents.SEARCH_UI_LOADED:
        return (
          <div></div>
        );
      case ProductSearchEvents.SEARCH_INITIATED:
        NotificationActions.notify(NotificationConstants.DISMISS, '');
        return (
          <div className="spinner">
            <div className="double-bounce1"></div>
            <div className="double-bounce2"></div>
          </div>
        );
      default:
        return this.getSearchResultsPanels(this.props.productSearchData);

    }
  }
}

export default ProductSearchResultGrid;
