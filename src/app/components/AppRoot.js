import React from '../../../node_modules/react/addons';

import Component from './Component';
import {RouteHandler} from 'react-router';

import Nav from './layout/Nav';
import Footer from './layout/Footer';

/*
 * @class AppRoot
 * @extends React.Component
 */
class AppRoot extends React.Component {

  constructor(props) {
    super(props);
  }

  /*
   * @method render
   * @returns {JSX}
   */
  render() {

    return (<div className="container">

      <Nav path={this.props.path}/>

      <div className="row marketing">
        <RouteHandler/>
      </div>

      <Footer />

    </div>);
  }
}

// Context types validation
AppRoot.childContextTypes = Component.contextTypes;

// Prop types validation
AppRoot.propTypes = {};

export default AppRoot;



