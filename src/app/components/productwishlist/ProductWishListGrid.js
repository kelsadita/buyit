import React from 'react';

import NotificationActions from './../../actions/NotificationActions';
import NotificationConstants from './../../constants/NotificationConstants';
import ProductWishListPanel from './ProductWishListPanel';

class ProductWishListGrid extends React.Component{
  constructor(props) {
    super(props);
  }

  static getSearchedProductResultListGrid(productsData) {
    let wishListProductsList = productsData || [];
    if (wishListProductsList.length === 0) {
      NotificationActions.notify(NotificationConstants.FAILED,
        'Oops! no product in wish list.');
    } else {
      NotificationActions.notify(NotificationConstants.DISMISS);
    }
    return wishListProductsList.map((product, index)=>
      <ProductWishListPanel productDetails={product} key={index}/>);
  }

  render() {
    return (
      <div>
        {ProductWishListGrid.getSearchedProductResultListGrid(this.props.productsData)}
      </div>
    );
  }
}

export default ProductWishListGrid;

