import React from 'react';
import ProductWishListActions from './../../actions/ProductWishListActions';
import ProductWishListStore from './../../stores/ProductWishListStore';

import ProductWishListGrid from './ProductWishListGrid';
import Notification from './../layout/Notification';

class ProductWishList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {productsData: props.productsData};
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    ProductWishListActions.loadAllProducts();
    ProductWishListStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  _onChange () {
    if(this.mounted) {
      this.setState({productsData: ProductWishListStore.getState()});
    }
  }

  render() {
    return(
      <div>
        <Notification />
        <ProductWishListGrid productsData={this.state.productsData}/>
      </div>
    );
  }
}

ProductWishList.propTypes = {
  productsData: React.PropTypes.object
};

ProductWishList.defaultProps = {
  productsData: ProductWishListStore.getState()
};

export default ProductWishList;

