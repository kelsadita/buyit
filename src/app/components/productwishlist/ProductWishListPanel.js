import React from 'react';
import {Link} from 'react-router';
import ProductWishListActions from './../../actions/ProductWishListActions';

class ProductWishListPanel extends React.Component{
  constructor(props) {
    super(props);
    this.imageStyle = {
      width: 280,
      height: 360
    };
    this.panelHeadingStyle = {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    };
  }
  removeFromWishList(productId) {
    ProductWishListActions.removeProductFromWishList(productId);
  }
  render() {
    let product = this.props.productDetails;
    return (
      <div className="col-md-6">
          <div className="panel panel-default">
            <div className="panel-heading" style={this.panelHeadingStyle}>{product.title}</div>
            <div className="panel-body">
              <Link to={"/product/" + product.productId}>
                <img src={product.imageUrl} alt={product.title} style={this.imageStyle} className="img-thumbnail"/>
              </Link>
              <hr/>
              <div className="col-md-7">
                <h4>{product.price[0].FormattedPrice[0]}</h4>
              </div>
              <div className="col-md-5">
                <button className="btn btn-danger" onClick={this.removeFromWishList.bind(this, product.productId)}>
                  <span className="glyphicon glyphicon-trash" aria-hidden="true">&nbsp;</span>
                  Delete
                </button>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default ProductWishListPanel;
