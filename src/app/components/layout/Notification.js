import React from 'react';
import NotificationStore from './../../stores/NotificationStore';
import NotificationConstants from './../../constants/NotificationConstants';

class Notification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notificationData: props.notificationData
    };
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    NotificationStore.addChangeListener(this._onChange)
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  _onChange() {
    if (this.mounted) {
      this.setState({
        notificationData: NotificationStore.getState()
      });
    }
  }

  static getNotificationClass(notificationType) {
    switch (notificationType) {
      case NotificationConstants.SUCCESS:
        return "alert-success";
      case NotificationConstants.FAILED:
        return "alert-danger";
      case NotificationConstants.WARNING:
        return "alert-warning";
      default:
        return "alert-success";
    }
  }

  render() {
    if(this.state.notificationData.type !== NotificationConstants.DISMISS) {

      let notificationData = this.state.notificationData;
      let notificationClass = Notification.getNotificationClass(this.state.notificationData.type);
      return (
        <div className={"alert " + notificationClass} role="alert">
          <span dangerouslySetInnerHTML={{__html: notificationData.message}} />
          <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      );
    } else {
      return (<div></div>);
    }

  }
}

Notification.propTypes = {
  notificationData: React.PropTypes.object
};

Notification.defaultProps = {
  notificationData: NotificationStore.getState()
};

export default Notification;
