import React from 'react';

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <footer className="footer">
        <p>&copy; Kelsadita 2015</p>
      </footer>
    );
  }
}

export default Footer;
