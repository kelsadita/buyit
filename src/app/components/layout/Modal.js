import React from 'react';

class Modal extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="modal fade" id={this.props.modalId} tabIndex="-1" role="dialog">
        <div className="modal-dialog modal-md" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title" id="addSavingModalLabel">{this.props.modalTitle}</h4>
            </div>
            <div className="modal-body">
              {this.props.modalBody}
            </div>
            <div className="modal-footer">
              {this.props.modalFooter}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal