import React from 'react';
import {Link} from 'react-router';

class Nav extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {
    let currentPath = this.props.path;
    return (
      <div className="header clearfix">
        <nav>
          <ul className="nav nav-pills pull-right">
            <li role="presentation"
                className={currentPath === "/" ? "active" : ""}>
              <Link to="/">Search</Link>
            </li>
            <li role="presentation"
                className={(currentPath === "/wishlist" || /\/product\/.*?/.test(currentPath)) ? "active" : ""}>
              <Link to="/wishlist">WishList</Link>
            </li>
          </ul>
        </nav>
        <h3 className="text-muted">BuyIt</h3>
      </div>
    );
  }
}

export default Nav;