import React from 'react';

class ProductSavingProgress extends React.Component {
  constructor(props) {
    super(props);
  }

  getTotalSaving(productSavings) {
    if (Array.isArray(productSavings) &&
        productSavings.length > 0 &&
        productSavings[0].savings.length > 0) {
      let savings = productSavings[0].savings;
      return savings.map((saving) => {
        return saving.amount;
      }).reduce((acc, currentSaving) => {
        return parseInt(acc) + parseInt(currentSaving);
      });
    }
    return 0;
  }

  getPercentageSavingProgress() {
    let totalSaving = this.getTotalSaving(this.props.productSavings);
    let productPrice = this.props.productPrice;
    return productPrice == 0 ? 0 : Math.ceil((totalSaving / productPrice) * 100);
  }

  getProgressBarClass(percentSavingProgress) {
    if (percentSavingProgress < 35) {
      return 'progress-bar progress-bar-danger';
    } else if (percentSavingProgress >= 35 && percentSavingProgress < 70) {
      return 'progress-bar progress-bar-warning';
    } else {
      return 'progress-bar progress-bar-success';
    }
  }

  render() {

    let percentSavingProgress = this.getPercentageSavingProgress();
    percentSavingProgress = percentSavingProgress > 100 ? 100 : percentSavingProgress;
    let progressBarStyle = {
      'width': percentSavingProgress + '%'
    };
    let progressBarClass = this.getProgressBarClass(percentSavingProgress);

    return (
      <div>
        <div className="progress">
          <div className={progressBarClass}
               role="progressbar"
               aria-valuenow="40"
               aria-valuemin="0"
               aria-valuemax="100"
               style={progressBarStyle}>
            {percentSavingProgress + '%'}
          </div>
        </div>
      </div>
    );
  }
}

export default ProductSavingProgress;