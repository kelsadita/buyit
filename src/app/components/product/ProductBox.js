import React from 'react';

class ProductBox extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    if (!this.props.productDescription) {
      return (<div></div>);
    }

    let productDescription = this.props.productDescription,
      imageUrl = productDescription.imageUrl,
      productPrice = productDescription.price[0].FormattedPrice[0],
      productTitle = productDescription.title;

    let productImageStyle = {
      height: '350px'
    };

    return (
      <div className="panel panel-default">

        <div className="panel-heading">
          <h4>{productTitle}</h4>
        </div>

        <div className="panel-body">
          <div className="row">
            <div className="col-sm-12 col-md-12">
              <div className="thumbnail">
                <img src={imageUrl} alt={productTitle} style={productImageStyle}/>

                <div className="caption">
                  <h2 className="text-center">{productPrice}</h2>

                  <p></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductBox;