import React from 'react';
import ProductActions from './../../actions/ProductActions';
import DateTimeField from 'react-bootstrap-datetimepicker';
import Modal from './../layout/Modal';
import moment from 'moment';

class AddSavingModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: props.date,
      amount: props.amount,
      comment: props.comment
    };
    this.addNewProductSaving = this.addNewProductSaving.bind(this)
    this.handleChange = this.handleChange.bind(this);
  }

  resetAddSavingFormState() {
    this.setState({
        date: moment().format('DD/MM/YYYY'),
        amount: 0,
        comment: ''
    });
  }

  handleChange(event) {
    let value = typeof event === 'string' ? event : event.target.value;
    let propertyName = typeof event === 'string' ? 'date' : event.target.name;
    this.setState((previousState) => {
      previousState[propertyName] = value;
      return { previousState: previousState };
    });
  }

  addNewProductSaving() {
    // TODO: try to remove dependency on jQuery
    ProductActions.addProductSaving(this.props.productId, {
      date: this.state.date,
      amount: this.state.amount,
      comment: this.state.comment
    });
    this.resetAddSavingFormState();
    $('#closeAddSavingModalBtn').click();
  }

  getAddSavingForm() {
    return (
      <div>
        <form className="form-horizontal">
          <div className="form-group">
            <label htmlFor="date" className="col-sm-2 control-label">Date </label>

            <div className="col-sm-10">
              <DateTimeField name="date"
                             format="DD/MM/YYYY"
                             inputFormat="DD/MM/YYYY"
                             dateTime={this.state.date}
                             onChange={this.handleChange}
                             mode="date"
                             placeholderText="Date">
                <input type="text" className="form-control add-saving-form-input" id="date"/>
               </DateTimeField>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="amount" className="col-sm-2 control-label">Amount</label>

            <div className="col-sm-10">
              <input type="text" className="form-control add-saving-form-input" id="amount" placeholder="Amount"
                     name="amount"
                     value={this.state.amount}
                     onChange={this.handleChange}/>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="comment" className="col-sm-2 control-label">Comment</label>

            <div className="col-sm-10">
              <input type="text" className="form-control add-saving-form-input" id="comment" placeholder="Comment"
                     name="comment"
                     value={this.state.comment}
                     onChange={this.handleChange}/>
            </div>
          </div>
        </form>
      </div>
    );
  }

  getAddSavingFooter() {
    return (
      <div>
        <button type="button"
                className="btn btn-default"
                data-dismiss="modal"
                id="closeAddSavingModalBtn">Close</button>
        <button type="button"
                className="btn btn-primary"
                onClick={this.addNewProductSaving}>Add</button>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Modal
          modalId="addSavingModal"
          modalTitle="Add Saving"
          modalBody={this.getAddSavingForm()}
          modalFooter={this.getAddSavingFooter()}
          />
      </div>
    );
  }
}

AddSavingModal.propTypes = {
  date: React.PropTypes.string,
  amount: React.PropTypes.number,
  comment: React.PropTypes.string
};

AddSavingModal.defaultProps = {
  date: moment().format('DD/MM/YYYY'),
  amount: 0,
  comment: ''
};

export default AddSavingModal;