import React from 'react';
import ProductSavingStore from './../../stores/ProductSavingStore';
import ProductActions from './../../actions/ProductActions'

import ProductSavingProgress from './ProductSavingProgress';

class ProductSavings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {productSaving: props.productSaving};
    this._onChange = this._onChange.bind(this);
    this.deleteSaving = this.deleteSaving.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    ProductSavingStore.addChangeListener(this._onChange);
    ProductActions.loadProductSavings(this.props.productId);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  deleteSaving(savingId) {
    ProductActions.deleteProductSaving(this.props.productId, savingId)
  }

  _onChange() {
    if(this.mounted) {
      this.setState({productSaving: ProductSavingStore.getState()});
    }
  }

  getProductSavingTable(productSavingData) {

    if (Array.isArray(productSavingData) && productSavingData.length > 0) {
      let productSavingDetails = productSavingData[0];
      return productSavingDetails.savings.map((saving, index) => {
        return (
          <tr>
            <td className="col-md-1">{index + 1}</td>
            <td className="col-md-1">{saving.date}</td>
            <td className="col-md-2">{saving.amount}</td>
            <td className="col-md-6">{saving.comment}</td>
            <td className="col-md-2">
              <span className="glyphicon glyphicon-pencil product-saving-control-icon"
                    aria-hidden="true"
                    data-target="#editSavingModal"
                    data-product-id={productSavingDetails.productId}
                    data-saving-details={JSON.stringify(saving)}
                    data-toggle="modal"></span>
              <span className="glyphicon glyphicon-trash product-saving-control-icon"
                    aria-hidden="true"
                    onClick={this.deleteSaving.bind(this, saving._id)}></span>
            </td>
          </tr>
        )
      });
    }
    return '';
  }

  render() {
    return (
      <div>

        <ProductSavingProgress
          productPrice={this.props.productPrice}
          productSavings={this.state.productSaving}/>

        <table className="table table-hover">
          <thead>
          <tr>
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Comment</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          {this.getProductSavingTable(this.state.productSaving)}
          </tbody>
        </table>
      </div>
    );
  }
}

ProductSavings.propTypes = {productSaving: React.PropTypes.object};
ProductSavings.defaultProps = {productSaving: ProductSavingStore.getState()};

export default ProductSavings;
