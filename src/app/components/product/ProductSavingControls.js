import React from 'react';
import AddSavingModal from './AddSavingModal';
import EditSavingModal from './EditSavingModal';

class ProductSavingControls extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (<div className="col-md-12">
      <div className="col-md-6">
        <button type="button" className="btn btn-success btn-block" data-toggle="modal" data-target="#addSavingModal">
          Add Saving
        </button>
      </div>
      <div className="col-md-6">
        <button type="button" className="btn btn-danger btn-block">Add Debt</button>
      </div>
      <AddSavingModal productId={this.props.productId}/>
      <EditSavingModal />
    </div>);
  }
}

export default ProductSavingControls;