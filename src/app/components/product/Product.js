import React from '../../../../node_modules/react/addons';
import ProductActions from './../../actions/ProductActions'
import ProductStore from './../../stores/ProductStore';
import ProductBox from './ProductBox';
import ProductSavings from './ProductSavings';
import ProductSavingControls from './ProductSavingControls';
import {Link} from 'react-router';

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = {productDescription: props.productDescription};
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    let productId = this.props.params.productId;
    ProductStore.addChangeListener(this._onChange);
    ProductActions.loadProduct(productId);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  _onChange() {
    if(this.mounted) {
      this.setState({productDescription: ProductStore.getState()});
    }
  }

  static getProductPrice(productPriceData) {
    if (!productPriceData) {
      return 0;
    }
    let amountString = productPriceData.price[0].Amount[0];
    return parseInt(amountString.slice(0, amountString.length - 2));
  }

  render() {
    let productPrice = Product.getProductPrice(this.state.productDescription);
    return (
      <div>
        <Link to="/wishlist">
          <button className="btn btn-success">
            <span className="glyphicon glyphicon-arrow-left" aria-hidden="true">&nbsp;</span>
            Back to wish list
          </button>
        </Link>
        <hr/>
        <ProductBox productDescription={this.state.productDescription}/>
        <ProductSavings productId={this.props.params.productId} productPrice={productPrice}/>
        <ProductSavingControls productId={this.props.params.productId} />
      </div>
    );
  }
}

Product.propTypes = {productDescription: React.PropTypes.object};
Product.defaultProps = {productDescription: ProductStore.getState()};

export default Product;
