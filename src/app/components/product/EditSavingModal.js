import React from 'react';
import moment from 'moment';

import ProductSavingActions from './../../actions/ProductActions';

import Modal from './../layout/Modal';
import DateTimeField from 'react-bootstrap-datetimepicker';


class EditSavingModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: props.date,
      amount: props.amount,
      comment: props.comment,
      savingId: props.savingId,
      productId: props.productId
    };
    this.editProductSaving = this.editProductSaving.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    let self = this;
    $('#editSavingModal').on('show.bs.modal', (event) => {
      let button = $(event.relatedTarget);
      let savingDetails = button.data('saving-details');
      let productId = button.data('product-id');
      self.setState({
        date: savingDetails.date,
        amount: savingDetails.amount,
        comment: savingDetails.comment,
        savingId: savingDetails._id,
        productId: productId
      });
      button.removeData();
    });
  }

  editProductSaving() {
    ProductSavingActions.editProductSaving(this.state);
    document.getElementById('closeEditSavingModalBtn').click();
  }

  handleChange(event) {
    let value = typeof event === 'string' ? event : event.target.value;
    let propertyName = typeof event === 'string' ? 'date' : event.target.name;
    this.setState((previousState) => {
      previousState[propertyName] = value;
      return { previousState: previousState };
    });
  }

  getEditSavingForm() {
    return (
      <div>
        <form className="form-horizontal">
          <div className="form-group">
            <label htmlFor="date" className="col-sm-2 control-label">Date </label>

            <div className="col-sm-10">
              <DateTimeField name="date"
                             format="DD/MM/YYYY"
                             inputFormat="DD/MM/YYYY"
                             dateTime={this.state.date}
                             onChange={this.handleChange}
                             mode="date"
                             placeholderText="Date">
                <input type="text" className="form-control add-saving-form-input" id="date"/>
              </DateTimeField>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="amount" className="col-sm-2 control-label">Amount</label>

            <div className="col-sm-10">
              <input type="text" className="form-control add-saving-form-input" id="amount" placeholder="Amount"
                     name="amount"
                     value={this.state.amount}
                     onChange={this.handleChange}/>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="comment" className="col-sm-2 control-label">Comment</label>

            <div className="col-sm-10">
              <input type="text" className="form-control add-saving-form-input" id="comment" placeholder="Comment"
                     name="comment"
                     value={this.state.comment}
                     onChange={this.handleChange}/>
            </div>
          </div>
        </form>
      </div>
    );
  }

  getAddSavingFooter() {
    return (
      <div>
        <button type="button"
                className="btn btn-default"
                data-dismiss="modal"
                id="closeEditSavingModalBtn">Close</button>
        <button type="button"
                className="btn btn-primary"
                onClick={this.editProductSaving}>Edit</button>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Modal
          modalId="editSavingModal"
          modalTitle="Edit Saving"
          modalBody={this.getEditSavingForm()}
          modalFooter={this.getAddSavingFooter()}
          />
      </div>
    );
  }
}

EditSavingModal.propTypes = {
  date: React.PropTypes.string,
  amount: React.PropTypes.number,
  comment: React.PropTypes.string,
  productId: React.PropTypes.string,
  savingId: React.PropTypes.string
};

EditSavingModal.defaultProps = {
  date: moment().format('DD/MM/YYYY'),
  amount: 0,
  comment: '',
  savingId: '',
  productId: ''
};

export default EditSavingModal;