import React from 'react';
import {Route, DefaultRoute} from 'react-router';
import AppRoot from './../components/AppRoot';

// components
import Product from '../components/product/Product';
import ProductSearch from '../components/productsearch/ProductSearch';
import ProductWishList from '../components/productwishlist/ProductWishList';

let routes = (
  <Route handler={AppRoot}>
    <DefaultRoute handler={ProductSearch}/>
    <Route path="/" handler={ProductSearch}/>
    <Route path="/wishlist" handler={ProductWishList}/>
    <Route path="product/:productId" handler={Product}/>
  </Route>
);

export default routes;