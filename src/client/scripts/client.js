import React from 'react';
import routes from './../../app/routes/routes';

import Router from 'react-router';

window.jQuery = window.$ =  require('jquery/dist/jquery.min');
require('bootstrap');

var attachElement = document.getElementById('app');

Router.run(routes, Router.HashLocation, (Root, state) => {
  React.render(<Root {...state}/>, attachElement);
});